﻿#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "This is the Animal class" << endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woff!" << endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Bird : public Animal
{
public:
	void Voice() override
	{
		cout << "Chirp!" << endl;
	}
};

int main()
{
	Animal* arr[3];

	arr[0] = new Dog;
	arr[1] = new Cat;
	arr[2] = new Bird;

	for (int i = 0; i < 3; i++)
	{
		arr[i]->Voice();
	}
}
